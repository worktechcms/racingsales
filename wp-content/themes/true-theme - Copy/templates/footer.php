<footer class="content-info" role="contentinfo">
  <div class="container">

  	<div class="row">
  		<div class="col-sm-6 text-center-xs">
  			<div class="copy">
  				<? = get_field('footer_copyright', 'option') ?>
  			</div>
  		</div>
  		<div class="col-sm-6 text-center-xs">
        <div class="true-footer-block">
            <a href="http://trueagency.com.au" title="Web Design Melbourne" target="_blank">
                <div class="normal-text">
                    Web Design by <img src="<?= TrueLib::getImageURL('common/true-footer-logo.png') ?>" alt="True Agency" class="retina-image" />
                </div>
                <div class="hover-text">trueagency.com.au</div>
            </a>
        </div>
      
  			<p class="text-true-footer"> Web Design by  <a href="http://www.trueagency.com.au" class="true-link" title="Web Design Melbourne" target="_blank"><img src="<?= TrueLib::getImageURL('true-logo.png') ?>" class="true-logo retina-image" alt="True Agency"> True Agency</a></p>
  		</div>
  	</div>
    <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
  </div>
</footer>

<?php wp_footer(); ?>
