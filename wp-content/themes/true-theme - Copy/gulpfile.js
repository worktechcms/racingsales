var gulp      	  = require('gulp'),
	less          = require('gulp-less'),
	plumber       = require('gulp-plumber'),
	autoprefixer  = require('gulp-autoprefixer'),
	minifycss     = require('gulp-minify-css'),
	jshint        = require('gulp-jshint'),
	uglify        = require('gulp-uglify'),
	rename        = require('gulp-rename'),
	concat        = require('gulp-concat'),
	notify        = require('gulp-notify'),
	livereload    = require('gulp-livereload'),
	// sourcemaps = require('gulp-sourcemaps'),
	del           = require('del');

var f = require('./gulp-config.json');

// Prevent watch from terminating when there is error
var onError = function(err) {
    notify.onError({
                title:    "Error",
                subtitle: "Abort, abort!",
                message:  "Error: <%= error.message %>",
                sound:    "Beep"
            })(err);

    this.emit('end');
};

/**
 * LESS compiler
 */
gulp.task('less', function() {
	return gulp.src('assets/less/app.less')
				.pipe(plumber({errorHandler: onError}))
				// .pipe(sourcemaps.init())
					.pipe(less())
					.pipe(rename('main.min.css'))
					.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
					.pipe(minifycss())
				// .pipe(sourcemaps.write('.'))
				.pipe(gulp.dest('assets/css/'))
				.pipe(livereload())
				.pipe(notify({ message: 'Less Files Compiled' }));
});

/**
 * CSSMin compiler
 */
gulp.task('cssmin', function() {
	return gulp.src('assets/vendor/*/css/**/*.css')
				.pipe(plumber({errorHandler: onError}))
				.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
				.pipe(concat('vendor.min.css'))
				.pipe(minifycss())
				.pipe(gulp.dest('assets/css/'))
				.pipe(livereload())
				.pipe(notify({ message: 'CSS Compiled' }));
});

/**
 * Script (App) compiler
 */
gulp.task('scripts', function() {
	return gulp.src(f.uglify.app)
				.pipe(plumber({errorHandler: onError}))
				.pipe(jshint())
			    .pipe(jshint.reporter('default'))
				.pipe(concat('main.js'))
				.pipe(rename({ suffix: '.min' }))
				.pipe(uglify({
					mangle: false,
					compress: false
				}))
				.pipe(gulp.dest('assets/js/dist/'))
				.pipe(livereload())
				.pipe(notify({ message: 'Javascript Compiled' }));
});

/**
 * Script (Vendor) compiler
 */
gulp.task('scripts_vendor', function() {
	return gulp.src(f.concat.vendor)
				.pipe(plumber({errorHandler: onError}))
				.pipe(concat('scripts.js'))
				.pipe(uglify())
				.pipe(rename({ suffix: '.min' }))
				.pipe(gulp.dest('assets/js/dist/'))
				.pipe(notify({ message: 'Vendor JS Compiled' }));
});

// Default task
gulp.task('default', ['less', 'scripts', 'scripts_vendor', 'cssmin', 'watch'], function() {

});

// I'm watching you
gulp.task('watch', function() 
{
	// Watching Less files
	gulp.watch(['assets/less/app.less','assets/less/**/*.less', 'assets/vendor/*/less/**/*.less', 'gulp-config.json'], ['less']);

	// Watching CSS min files
	gulp.watch(['assets/vendor/**/*.css', 'gulp-config.json'], ['cssmin']);

	// Watching Script (App) files
	gulp.watch([f.uglify.app, 'gulp-config.json'], ['scripts']);

	// Watching Less files
	gulp.watch([f.concat.vendor, 'gulp-config.json'], ['scripts_vendor']);

});