(function($) {

    $('.racingsales-horse-gallery__search__field').live('keyup', function() {
        var filter = new RegExp( $(this).val(), 'gi' );

        $(this).closest('.racingsales-horse-gallery').find('.racingsales-horse-gallery__list').isotope({
            layoutMode: 'fitRows',
            filter: function() {
                return filter ? $(this).text().match( filter ) : true;
            }
        });
    });

    $('.racingsales-horse-gallery__categories a').live('click', function(e) {
        e.preventDefault();

        var filter = $(this).find('.filter').html();

        $(this).closest('.racingsales-horse-gallery').find('.racingsales-horse-gallery__list').isotope({
            layoutMode: 'fitRows',
            filter: filter
        });
    });

})(jQuery);
