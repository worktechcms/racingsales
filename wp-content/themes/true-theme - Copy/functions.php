<?php
define('THEME_NAME', 'true-theme-copy');

/**
 * Roots includes
 */
require_once locate_template('/app/roots/utils.php');           // Utility functions
require_once locate_template('/app/roots/init.php');            // Initial theme setup and constants
require_once locate_template('/app/roots/wrapper.php');         // Theme wrapper class
require_once locate_template('/app/roots/sidebar.php');         // Sidebar class
require_once locate_template('/app/roots/config.php');          // Configuration
require_once locate_template('/app/roots/titles.php');          // Page titles
require_once locate_template('/app/roots/cleanup.php');         // Cleanup
require_once locate_template('/app/roots/nav.php');             // Custom nav modifications
require_once locate_template('/app/roots/gallery.php');         // Custom [gallery] modifications
require_once locate_template('/app/roots/comments.php');        // Custom comments modifications
require_once locate_template('/app/roots/relative-urls.php');   // Root relative URLs
require_once locate_template('/app/roots/widgets.php');         // Sidebars and widgets
require_once locate_template('/app/roots/scripts.php');         // Scripts and stylesheets
require_once locate_template('/app/roots/custom.php');          // Custom functions

// require_once 'vendor/autoload.php';

require_once 'app/core.php';

/**
 * Enqueue Scripts/Styles
 */
function racingsales_enqueue_scripts() {

    // if ( ! is_admin() )
    //     wp_dequeue_script( 'jquery' );
    //
    // wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js' );

    wp_enqueue_style( 'racingsales-gallery', get_stylesheet_directory_uri() . '/gallery.css' );
    wp_enqueue_style( 'racingsales-home', get_stylesheet_directory_uri() . '/assets/css/home.css' );

    wp_enqueue_script( 'racingsales-isotope', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.4/isotope.pkgd.min.js', array('jquery') );
    wp_enqueue_script( 'racingsales-gallery', get_stylesheet_directory_uri() . '/assets/js/gallery.js', array('jquery','racingsales-isotope') );
}
add_action( 'wp_enqueue_scripts', 'racingsales_enqueue_scripts' );

function racingsales_pre_get_posts( $query ){
    // if ( ! is_admin()
    //     && $query->is_post_type_archive( 'product' )
    //     && $query->is_main_query() ) {
    //         $query->set( 'posts_per_page', 3 );
    // }

    // if ( ! is_admin() && $query->is_main_query() ) {
            $query->set( 'posts_per_page', 24 );
    // }
}
add_action( 'pre_get_posts', 'racingsales_pre_get_posts' );

/**
 * Shortcode Gallery
 */
function racingsales_horse_gallery( $atts ) {
    // if ( ! current_user_can('manage_options') )
    //     return '';

    extract(shortcode_atts(array(
    ), $atts));

    $horses = new WP_Query(array(
        'post_type' => 'rs_horse',
        'posts_per_page' => -1
    ));

    $horse_categories = get_terms('rs_horse_category', array(
        'hide_empty' => false,
    ));

    ob_start();

    ?>

    <div class="racingsales-horse-gallery">
        <div class="racingsales-horse-gallery__search">
            <input type="text" class="racingsales-horse-gallery__search__field" placeholder="Search Horses..."/>
        </div>
        <div class="racingsales-horse-gallery__categories">
            <a href="#" class="racingsales-horse-gallery__categories__link">All<span class="filter">*</span></a>
            <?php foreach ( $horse_categories as $category ) : ?>
                <a href="#" class="racingsales-horse-gallery__categories__link"><?php echo $category->name; ?><span class="filter">.racingsales-horse-gallery__list__item--<?php echo $category->slug; ?></span></a>
            <?php endforeach; ?>
        </div>
        <div class="racingsales-horse-gallery__list">
            <?php if ( $horses->have_posts() ) : ?>
                <?php while ( $horses->have_posts() ) : $horses->the_post();
                    $terms = get_the_terms( get_the_id(), 'rs_horse_category' );

                    $term_list = array();
                    if ( $terms ) {
                        foreach ( $terms as $term ) {
                            $term_list[] = 'racingsales-horse-gallery__list__item--' . $term->slug;
                        }
                    }

                    $term_classes = '';
                    if ( $term_list) {
                        $term_classes = implode( ' ', $term_list );
                    }
                ?>
                    <div class="racingsales-horse-gallery__list__item racingsales-horse-gallery__list__item--<?php echo sanitize_title( get_the_title() ); ?> <?php echo $term_classes; ?>">
                        <a href="<?php the_field('post_link'); ?>" class="racingsales-horse-gallery__list__item__link">
                            <?php the_post_thumbnail( array(200, 200) ); ?>
                            <!-- <span class="racingsales-horse-gallery__list__item__link__image"></span> -->
                            <h2><?php the_title(); ?></h2>
                        </a>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
            <?php endif; ?>
        </div>
    </div>

    <?php

    return ob_get_clean();
}
add_shortcode( 'racingsales_horse_gallery', 'racingsales_horse_gallery' );
