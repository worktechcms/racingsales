<?php
    /* -------------------------------------------------- */
    /* TrueLib
    /* -------------------------------------------------- */
    /*
     * The holder of various functions that we use across our sites!
     */
    class TrueLib
    {
        static function printVar($var)
        {
            echo '<pre>';
            var_dump($var);
            echo '</pre>';
        }

        static function getThemeURL()
        {
            return get_template_directory_uri();
        }
        
        static function getThemeDir($path = '')
        {
            return get_template_directory() . $path;
        }
        
        static function getImageURL($filename = '')
        {
            return get_template_directory_uri() . '/assets/img/' . $filename;
        }
                
        static function getCSS($css)
        {
            return get_template_directory_uri() . '/assets/css/' . $css . '.css';
        }
        
        static function getJS($js)
        {
            return get_template_directory_uri() . '/assets/js/' . $js . '.js';
        }

        static function createSocialButton($title, $key, $image, $url, $suffix = '')
        {
            if(trim($url) != '')
            {
                ?>
                <li class="social-<?= $image ?>">
                    <a href="<?=$url?>" class="social-button <?=$image?>" target="_blank">
                        <img src="<?=self::getImageURL('social/social-' . $image . $suffix  . '.png')?>" alt="<?=$title?>" class="retina-image normal" />
                    </a>
                </li>
                <?php    
            }

        }
        
        static function printSocialButtons($suffix = '')
        {
            if(get_field('social_accounts', 'option'))
            {
                while(has_sub_field('social_accounts', 'option'))
                {
                    TrueLib::createSocialButton(ucfirst(strtolower(get_sub_field('account_type'))), 'social-' . get_sub_field('account_type'), get_sub_field('account_type'), get_sub_field('account_url'), 'suffix');
                }
            }
        }
        
        static function getFooterCopyright()
        {
            return str_replace('%year%', date('Y'), get_field('footer_copyright', 'option'));
        }

        static function getTemplatePart($name)
        {
            get_template_part('page-templates/partials/' . $name);
        }

        static function printSocialAwesome($iconModifier = '')
        {
            if(get_field('social_accounts', 'option'))
            {
                while(has_sub_field('social_accounts', 'option'))
                {  
                    $type = get_sub_field('account_type');
                    $url = get_sub_field('account_url');
                    $iconType = $type;
                    if ($type == 'youtube') {
                        $iconType = 'youtube-play';
                    }
                    if(trim($url) != '')
                    {
                        ?>
                        <li class="social-<?= $type ?>">
                            <a href="<?=$url?>" class="social-button <?=$type?>" target="_blank" title="<?= ucfirst($type) ?>">
                                <i class="fa fa-<?= $iconType ?> <?= $iconModifier ?>"></i>
                            </a>
                        </li>
                        <?php
                    }
                }
            }
        }       

        /* Get Image with ACF
        /* name = acf key / filename
        /* acf = true / false
        /* size = string
        /* subfield = true / false
        /* id = integer
        /* class = string
        /* retina = true / false - not applicable to acf
        /* -------------------------------------------------- */
        static function getImage($args)
        {
            $id = null;
            $acf = false;
            $class = '';
            $retina = false;
            $name = '';
            $subfield = false;
            $size = '';
            extract($args);

            if($acf)
            {
                if($id == null)
                {
                    if($subfield)
                    {
                        $image = get_sub_field($name);   
                    } else {
                        $image = get_field($name);  
                    }
                } else {
                    if($subField)
                    {
                        $image = get_sub_field($name, $id);   
                    } else {
                        
                        $image = get_field($name, $id);
                    } 
                }

                //If we have an image, display it!
                if($image)
                {
                    if($size != '')
                    {
                        $imageURL = $image['sizes'][$size];
                    } else {
                        $imageURL = $image['url'];
                    }
                    
                    $str = '<img ';
                    if($class != '')
                    {
                        $str .= 'class="' . $class . '"';
                    }
                    
                    return $str . ' src="' . $imageURL . '" alt="' . $image['alt'] . '">'; 
                }
            } else {
                if($retina)
                {
                    $class .= ' retina-image';
                }
                
                $str = '<img ';
                if($class != '')
                {
                    $str .= 'class="' . trim($class) . '"';
                  
                }
                return  $str . ' src="' . self::getImageURL($url) . '" alt="' . $alt  . '">';
            }
            return '';
        }
                
        static function getDescriptionField($field, $sub = false, $postID = null)
        {
            ?>
            <div class="page-description entry-content">
            <?php
                    if($sub)
                    {
                        echo get_sub_field($field);
                    } else {
                        echo get_field($field, $postID);
                    }
                ?>
            </div>
            <?php
        }
    }
