<?php

/**
 * 
 */

/**
 * ----------------------------------------------
 * 				Load required files 
 * ----------------------------------------------
 */

class CoreLoader {

	public function __construct() {

	}

	public static function init() {

		$themeRoot = self::themeRoot();

		/**
		 * ----------------------------------------------
		 * 				Load required files 
		 * ----------------------------------------------
		 */
		require_once $themeRoot . 'true/trueLib.php';
		require_once $themeRoot . 'true/BSHelper.php';

		require_once $themeRoot . 'theme.php';

		require_once $themeRoot . 'true/AdminStyles.php';

		// Helper file
		require_once $themeRoot . 'lib/helpers.php';

		/*
		|--------------------------------------------------------------------------
		| 				Load custom post-types, widgets and stuff
		|--------------------------------------------------------------------------
		*/	
		
		self::loadPostType('field-definition');

		/**
		 * ----------------------------------------------
		 * 				Load required modules 
		 * ----------------------------------------------
		 */

		/**
		 * [Models module]
		 * ---------------------------------
		 * 
		 * These models are eloquent based models
		 * illuminate/database must be in truetheme/composer.json and installed first
		 *
		 * Remove when Eloquent models are not used.
		 *
		 */
        // require_once $themeRoot . 'models/base.php';
        // require_once $themeRoot . 'models/CapsuleHelper.php';

        // Add more models below

        /**
		 * [Validation Module]
		 *
		 * Used in conjunction with Eloquent Models
		 */
		
		// require_once $themeRoot . 'lib/Validator.php';

		/** 
		 * 
		 * [Custom Admin MVC Module]
		 *
		 * If you don't use the Admin Module above
		 * you probably won't need to use this
		 *
		 * Allow MVC-like customisation 
		 *
		 */

		// require_once $themeRoot . 'lib/View.php';
		// require_once $themeRoot . 'lib/Route.php';

		/**
		 * [Custom Admin module]
		 * ---------------------------------
		 *
		 * Take Wordpress dashboard to the next level
		 *
		 * If the following module is used uncomment the 
		 * 'changeUrl' function far below
		 * 
		 */
		// require_once $themeRoot . 'trueadmin/base.php';


	}

	public static function themeRoot() {
		return get_template_directory() . '/app/';
	}

	public static function themeRootURL() {
		return get_template_directory_uri() . '/app/';
	}

	public static function loadWidget($widget)
    {
        require_once self::themeRoot() . 'widgets/' . $widget . '.php';
    }
    
    public static function loadPostType($postType)
    {
        require_once self::themeRoot() . 'post-types/' . $postType . '.php';
    }
}

// function changeUrl()
// {
//     $pageURL = 'http://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

//     if (!isset($_GET['page']))
//     	return;

// 	$intendedSection = $_GET['page'];

// 	if ($intendedSection == TrueAdminLoader::TA_SLUG)
// 		return;

// 	$baseURL = get_bloginfo('wpurl');
// 	$adminURL = '/wp-admin/admin.php?';
// 	$mainSlug = 'page=' . TrueAdminLoader::TA_SLUG;

// 	$redirectTo = $baseURL . $adminURL . $mainSlug . '&section=' . $intendedSection;

//     if (TrueAdminLoader::endsWith($pageURL, TrueAdminLoader::TA_SECTION_CLASS_SUFFIX))
//     {
//         header ('location:' . $redirectTo);
//     }
// }
// add_action( 'admin_menu', 'changeUrl' );

CoreLoader::init();