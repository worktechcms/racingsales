<?php
    /* -------------------------------------------------- */
    /* BSHelper - Bootsrap Helper
    /* -------------------------------------------------- */
    /*
     * Various utility functions to speed up bootstrap markups
     *
     * Based on Bootstrap 3
     */
    
    class BSHelper {

    	public static function badge($text, $type = 'info') {
    		return '<span class="badge badge-'.$type.'">' . $text . '</span>';
    	}

    	public static function badgeSuccess($text) {
    		return self::badge($text, 'success');
    	}

    	public static function badgeInfo($text) {
    		return self::badge($text, 'info');
    	}

    	public static function badgeDanger($text) {
    		return self::badge($text, 'danger');
    	}

    	public static function badgeWarning($text) {
    		return self::badge($text, 'warning');
    	}

    	public static function label($text, $type = 'info') {
    		return '<span class="label label-'.$type.'">' . $text . '</span>';
    	}

    	public static function labelSuccess($text) {
    		return self::label($text, 'success');
    	}

    	public static function labelInfo($text) {
    		return self::label($text, 'info');
    	}

    	public static function labelDanger($text) {
    		return self::label($text, 'danger');
    	}

    	public static function labelWarning($text) {
    		return self::label($text, 'warning');
    	}

    }