<?php


/**
 *
 * A class to render the templates, handles and assign variables
 * used in the view
 * 
 * @author Jofry HS
 */

class View {

	private static $vars;

	private static $activeMenu = null;

	private static $sideMenus = array();

	private static $notifications = array();

	private static $section = null;

	private static $formActions = array();

	// Render the View along with the variables
	// Should only be called once, if you need to call multiple template
	// consider using getAdminTemplate in your template files
	public static function render($templateName, $variables = array()) {

		// Ensuring we have access to the variables in template
		self::$vars = $variables;

		if (!isset($variables['breadcrumb']) && !isset($variables['title'])) {
			self::$vars['breadcrumb'] = array();
			self::$vars['title'] = '';
		}
		else {
			if (!isset($variables['breadcrumb'])) {
				self::$vars['breadcrumb'] = array();
			}
			if (!isset($variables['title'])) {
				self::$vars['title'] = 'Section';
			}
		}

		if (count(self::$formActions) > 0) {
			self::$vars['hasForm'] = true;
			foreach (self::$formActions as $key => $value) {
				self::$vars[$key] = $value;
			}
		}


		self::getAdminTemplate('admin-precontent');

		// Header
		self::getAdminTemplate('admin-header');

		// Main Template Content
		self::getAdminTemplate($templateName);

		
		self::getAdminTemplate('admin-postcontent');
	}

	/**
	 * Add form submitaction to the top of the header
	 * The action will be pulled to the right
	 * In actual implementation, the submit is delegated:
	 * 
	 * $('#page-submit').click(function() {
	 * 		$([$formID]).submit()
	 * });
	 */
	public static function addPageSubmit($formID, $label = 'Save', $url = false) {
		self::$formActions['submit'] = $formID;
		self::$formActions['submitLabel'] = $label;
		self::$formActions['submitAsURL'] = $url;
	}

	/**
	 * Add delete to the top of the header
	 * The action will be pulled to the right
	 */
	public static function addPageDelete($url, $label = 'Delete') {
		self::$formActions['delete'] = $url;
		self::$formActions['deleteLabel'] = $label;
	}

	/**
	 * Add create to the top of the header
	 * The action will be pulled to the right
	 */
	public static function addPageCreate($url, $label = 'Create') {
		self::$formActions['create'] = $url;
		self::$formActions['createLabel'] = $label;
	}

	/**
	 * Advanced Search Button
	 */
	public static function addPageAdvancedSearch($url, $label = 'Search') {
		self::$formActions['adSearch'] = $url;
		self::$formActions['adSearchLabel'] = $label;
	}

	/**
	 * Advanced Search Button
	 */
	public static function addPageGenericButton($url, $label = '') {
		self::$formActions['genericButton'] = $url;
		self::$formActions['genericButtonLabel'] = $label;
	}



	/**
	 * Add Searc to the top of the header
	 * The action will be pulled to the right
	 * Sample:
	 * <a href="#!" class="btn btn-info [$class]" 
		*					data-toggle="modal" 
		*					data-target="[$target]" 
		*					data-search-click-target="[targetAfter]"> 
		*					<i class="fa fa-search"></i> [$label] </a>
	 */
	public static function addPageSearch($class, $target, $targetAfter, $label = 'Search') {
		self::$formActions['search'] = $target;
		self::$formActions['searchClass'] = $class;
		self::$formActions['searchTargetAfter'] = $targetAfter;
		self::$formActions['searchLabel'] = $label;
	}


	// The same as render, but you pass the HTML string instead of template name
	public static function renderRaw($html) {
		self::getAdminTemplate('admin-precontent');

		// Raw HTML
		echo ($html);

		self::getAdminTemplate('admin-postcontent');
	}

	// Return you the content of the templates
	public static function getAdminTemplate($templateName, $extraVars = array()) {

		/**
		 * Locate Template ensure we can still use our variables in the template files
		 * @see http://keithdevon.com/passing-variables-to-get_template_part-in-wordpress/
		 */
		
		if (count(self::$vars) > 0) {
			extract(self::$vars);
		}

		// Extra vars which can be passed from anywhere to the template
		
		if (count($extraVars) > 0) {
			extract($extraVars);
		}

		// Are we loading Blade?
		try {
			require (locate_template('app/trueadmin/partials/' . $templateName . '.php'));
		} catch (Exception $ex) {
			echo 'Error loading the template: ' . $templateName;
			echo '<pre>';
			print_r(error_get_last());
			print_r($ex);
			echo '</pre>';
		}
	}

	// Return you the HTML Content of a template (it doesn't output the template)
	public static function getAdminTemplateHTML($templateName, $extraVars = array()) {

		/**
		 * Locate Template ensure we can still use our variables in the template files
		 * @see http://keithdevon.com/passing-variables-to-get_template_part-in-wordpress/
		 */
		
		if (count(self::$vars) > 0) {
			extract(self::$vars);
		}

		// Extra vars which can be passed from anywhere to the template
		
		if (count($extraVars) > 0) {
			extract($extraVars);
		}

		ob_start();

		require (locate_template('app/trueadmin/partials/' . $templateName . '.php'));

		return ob_get_clean();
	}



	// Add this section to the menu
	public static function addToMenu($title, $label, $url, $icon) {
		$menuArr = array(
				'title' => $title,
				'label' => $label,
				'url' => $url,
				'icon' => $icon
			);
		self::$sideMenus[] = $menuArr;
	}

	// Force the active menu to a section
	public static function addActiveMenu($title) {
		self::$activeMenu = $title;
	}

	// Check if given title should be marked as active menu
	public static function isActiveMenu($title) {
		if (Route::getSection() == $title)
			return true;

		/*if (self::$activeMenu != null && self::$activeMenu == $title) {
			return true;
		}*/

		return false;
	}	

	public static function getMenu() {
		return self::$sideMenus;
	}


	/**
	 * Adding and retrieving 'flash' notifications
	 */
	public static function addNotification($message, $type = 'success', $useSession = false) {

		if (!$useSession) {
			self::$notifications[] = array(
					'message' => $message,
					'type' => $type
				);
		}
		else {
			$_SESSION['notif']['message'] = $message;
			$_SESSION['notif']['type'] = $type;
		}
	}

	public static function getNotifications() {
		return self::$notifications;
	}

	public static function getSessionNotifications() {

		if (isset($_SESSION['notif'])) {
			return $_SESSION['notif'];
		}
		else {
			return NULL;
		}
	}

	public static function countNotifications() {
	
		if (isset($_SESSION['notif'])) {
			return 1;
		}
		else {
			return count(self::$notifications);
		}
	}

}