<?php 

/**
* TrueMailchimp
*
* A wrapper to simplify AETM specific operations around Mailchimp PHP Class
* Packagist - mailchimp/mailchimp
*/
class TrueMailchimp
{
    /**
     * Mailchimp client
     * @var Mailchimp
     * @see https://bitbucket.org/mailchimp/mailchimp-api-php
     */
    protected static $client = null;

    public static function init($key) {
        self::$client = new Mailchimp($key);
    }

    public static function subscribe($listId, $userEmail, $firstName = '', $lastName = '', $groups)
    {
        if (!$userEmail) {
            return false;
        }

        $mergeVars = [
            'FNAME' => $firstName,
            'LNAME' => $lastName,
            'groupings' => array(
                array(
                    'id' => 7753,
                    'groups' => $groups 
                )
            )
        ];

        try {
            $result = self::$client->lists->subscribe(
                $listId,
                ['email' => $userEmail],
                $mergeVars,
                'html', // email_type
                false, // double_optin - sets whether email confirmation is required
                true, // update_existing
                true // replace_interests
            );

        } catch(Exception $e) {
            error_log('A mailchimp error occured: ' . get_class($e) . ' - ' . $e->getMessage());
            // TrueLib::printVar($e);
            // die();
            return false;
        }

        return true;
    }

    public static function unsubscribe($listId, $userEmail)
    {
        if (!$userEmail) {
            return false;
        }

        try {
            $result = self::$client->lists->unsubscribe(
                $listId,
                ['email' => $userEmail]
            );
        } catch(Exception $e) {
            error_log('A mailchimp error occured: ' . get_class($e) . ' - ' . $e->getMessage());
            // TrueLib::printVar($e);
            // die();
            return false;
        }

        return true;
    }

}