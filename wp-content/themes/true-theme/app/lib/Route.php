<?php


/**
 *
 * A class to handle our custom admin page routing
 * Built on top of existing Wordpress admin URL
 *
 * Wordpress default Admin URL:
 * http://mysite.com/wp-admin/admin.php?page=[our-admin-page]
 *
 * Our Routed Admin URL:
 * http://mysite.com/wp-admin/admin.php?page=[our-admin-page]&section=[controller]&do=[controllerAction]
 * 
 * @author Jofry HS
 */

class Route {

	// Sections
	const ROUTE_SECTION_SUFFIX = 'Section';
	const ROUTE_SECTION_DEFAULT = 'Dashboard';
	const ROUTE_SECTION_DIR = '/sections';
	const ROUTE_SECTION_DEFAULT_ACTION = 'index';

	public static $actionExecuted = false;

	// The holy grail of this class
	// Determine and call the correct section and action
	public static function decide() {
		if (!self::$actionExecuted) {
			// Prevent double calling
			self::$actionExecuted = true;

			$section = '';
			$action = '';

			// Check if there is any action?
			$action = self::isAction() ? self::getAction() : self::ROUTE_SECTION_DEFAULT_ACTION;

			// Check if there is any section?
			$section = self::isSection() ? self::getSection() : self::ROUTE_SECTION_DEFAULT . self::ROUTE_SECTION_SUFFIX;

			// Strip slashes because WP likes those slashes
		    $_POST      = array_map('stripslashes_deep', $_POST);
		    $_GET       = array_map('stripslashes_deep', $_GET);
		    $_COOKIE    = array_map('stripslashes_deep', $_COOKIE);
		    $_REQUEST   = array_map('stripslashes_deep', $_REQUEST);

			// Run the controller for the section
			call_user_func(array($section, $action));
		}
	}

	// Check redirection is hooked much early on than the actual Route::decide() in TrueAdmin
	// When redirection is to be expected, call Route::decide before everything else.
	// 
	// A section method which do redirection should have _redir suffix in the action call
	// 
	public static function checkRedirection() {
		$haveRedir = substr(self::getAction(), -strlen('_redir')) === '_redir';
		if ($haveRedir) {
			self::decide();
		}
	}

	public static function setTitle($admin_title, $title) {
		if (is_admin()) {
			if (isset($_GET['page'])) {
				if ($_GET['page'] == TrueAdminLoader::TA_SLUG) 
				{
					return self::getSection() . ' | Prime ';
				}
			}
		}
		return $admin_title;
	}

	// Check if Section slug is present
	// If passed with variables will check if we are in that section
	public static function isSection() {
		if (self::getSection() == null)
			return false;
		return true;
	}

	// Retrieve the current section
	public static function isDash() {
		return self::getSection() == self::ROUTE_SECTION_DEFAULT . self::ROUTE_SECTION_SUFFIX ? true : false;
	}

	// Retrieve the current section
	public static function getSection() {
		if (isset($_GET['section'])) {
			if (!empty($_GET['section'])) {
				return $_GET['section'];
			} else {
				return null;
			}
		}
		else {
			return self::ROUTE_SECTION_DEFAULT . self::ROUTE_SECTION_SUFFIX;;
		}
	}

	// Check if Action slug is present
	// If passed with variables will check if equals to current action
	public static function isAction() {
		if (self::getAction() == null)
			return false;
		return true;
	}

	// Retrieve the current action
	public static function getAction() {
		if (isset($_GET['do'])) {
			if (!empty($_GET['do'])) {
				return $_GET['do'];
			} else {
				return null;
			}
		}
		else {
			return null;
		}
	}

	// Check if at the main Admin page 
	// i.e. No Section or Action is present
	public static function isAdmin() {
		if (isSection())
			return false;
		return true;
	}

	public static function isPost() {
		return $_SERVER['REQUEST_METHOD'] == 'POST';
	}
    
    public static function isPostBack()
    {
        if(self::isPost() && isset($_POST['is-post-back']))
        {
            return true;
        }   
        return false;
    }

	public static function redirectTo($section = '', $action = '') {
		$url = self::generateURL($section, $action);

		header('Location: ' . $url);
	}

	// Generate the URL for our custom admin
	// Without any parameters, will generate main admin page
	public static function generateURL($section = '', $action = '', $getData = array()) {
		$get = '?page=' . TrueAdminLoader::TA_SLUG;

		if (!empty($section)) {
			$get .= '&section=' . $section;

			if (!empty($action)) {
				$get .= '&do=' . $action;
			}
		}

		// Is there any additional data you would like to pass?
		if (count($getData) > 0) {
			foreach($getData as $key => $value) {
				// Sanitize them
				$key = preg_replace('/[^-a-zA-Z0-9_]/', '', $key);
				$value = preg_replace('/[^-a-zA-Z0-9_]/', '', $value);
				$get .= '&' . $key . '=' . $value;
			}
		}

		$baseUrl = get_bloginfo('wpurl') . '/wp-admin/admin.php'; 

		return $baseUrl . $get;
	}


	public static function shutdown() {
		$e = error_get_last();

		if ($e) {
			if (strpos($e['message'], 'exif_read_data') !== FALSE) {
				return;
			}
			else if ($e['type'] === E_WARNING) {
				if (strpos($e['message'], 'call_user_func') === FALSE) {
					return;
				}
				else {
					View::render('err/500', compact('e'));
				}
			}
			else {
				View::render('err/500', compact('e'));
			}
		}
	}
}