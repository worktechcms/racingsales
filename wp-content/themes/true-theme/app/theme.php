<?php

 /* TrueTheme
 /* -------------------------------------------------- */
    class TrueTheme
    {
        static function init()
        {
            // Add options page to ACF
            // @see http://www.advancedcustomfields.com/resources/upgrading-v4-v5/
            if(function_exists('acf_add_options_page')) {

                acf_add_options_page();

                // Additional options pages goes here
                acf_add_options_page(array(
                    'page_title' => 'Navigation',
                    'icon_url'    => TrueLib::getImageURL('true-icons/trueMenuWPIcons.png'),
                ));
            }

            add_filter( 'wpseo_canonical', '__return_false' );
            add_filter('wpseo_robots', array(__CLASS__, 'removeRobots'));

            add_image_size('full-width', 1920, 999999);
            add_image_size('half-width', 960, 9999);

            add_image_size('main-banner-image', 1920, 885);
            add_image_size('page-banner-image', 1920, 267, true);

            add_action('wp_enqueue_scripts', array(__CLASS__, 'addScripts'), 100);
            add_filter('body_class', array(__CLASS__, 'addBodyClass'), 999 );

            add_action('login_head', array(__CLASS__, 'loginStyles'));

            // Hook into Roots Nav Walker
            add_filter('roots_wp_nav_menu_item', array(__CLASS__, 'addDropdownMenu'), 999, 2);


            add_action( 'init', array(__CLASS__, 'register_my_menus'));
            add_filter('wpcf7_ajax_loader', array(__CLASS__, 'wpcf7AjaxLoader' ));
            add_action( 'admin_bar_menu', array(__CLASS__, 'toolbar_link_to_mypage'), 999 );

            add_filter('acf/load_field/name=home_featured_seller', array(__CLASS__, 'acf_load_color_field_choices'));


            add_action('wpcf7_mail_sent', array(__CLASS__, 'handleEnquiryFormSent'));
        }

        static function handleEnquiryFormSent($cf7)
        {
            $submission = \WPCF7_Submission::get_instance();

            if ( $submission ) 
            {
                $formData = $submission->get_posted_data();

                if($formData['_wpcf7'] == 18)
                {   
                    //First Name
                    //Last Name
                    //Email
                    //Groups
                    $firstName = $formData['first-name'];
                    $lastName = $formData['last-name'];
                    $email = $formData['your-email'];

                    /* Newsletter Groups
                    /* ----------------------------- */
                    $groups = [];

                    //Should we signup the user to the newsletter?
                    if(isset($formData['newsletter'][0]) && $formData['newsletter'][0] != '') {

                        if (isset($formData['news_thoroughbred'][0]) && $formData['news_thoroughbred'][0] != '') {
                            $groups[] = 'Thoroughbreds';
                        }

                        if (isset($formData['news_greyhound'][0]) && $formData['news_greyhound'][0] != '') {
                            $groups[] = 'Greyhounds';
                        }

                        if (isset($formData['news_harness'][0]) && $formData['news_harness'][0] != '') {
                            $groups[] = 'Harness';
                        }

                        if (isset($formData['news_betting'][0]) && $formData['news_betting'][0] != '') {
                            $groups[] = 'Betting';
                        }

                        require get_template_directory() . '/vendor/autoload.php';
                        require get_template_directory() . '/app/lib/mailchimp.php';
                        TrueMailchimp::init('204efc4e2fb8a33be13739c6e0be8bff-us11'); //Account ID
                        TrueMailchimp::subscribe('fcfda8ddbd', $email, $firstName, $lastName, $groups);
                    }
                }         

                
            } else {
                return;
            }
        }


        static function acf_load_color_field_choices( $field )
        {
            // reset choices
            $field['choices'] = array();

            global $wpdb;
            $choices = $wpdb->get_results( 'SELECT * FROM users ORDER BY first_name', OBJECT );

            // loop through array and add to field 'choices'
            if( is_array($choices) ) {

                foreach( $choices as $choice ) {

                    $field['choices'][ $choice->id ] = $choice->first_name . " " . $choice->last_name . " - "  . $choice->business_name;

                }

            }

            // return the field
            return $field;

        }

        static function removeRobots()
        {
            return '';
        }


        static function toolbar_link_to_mypage( $wp_admin_bar ) {
            $args = array(
                'id'    => 'my_page',
                'title' => 'Prime Management',
                'href'  => '/prime/',
                'meta'  => array( 'class' => 'my-toolbar-page')
            );
            $wp_admin_bar->add_node( $args );
        }

        static function wpcf7AjaxLoader ()
        {
            return  get_bloginfo('stylesheet_directory') . '/assets/img/contact-form-loader.gif';
        }

        static function register_my_menus() {
            register_nav_menus(
                array(
                    'lower-footer-menu' => __( 'Lower Footer' ),
                    'upper-footer-menu' => __( 'Upper Footer Menu' )
                )
            );
        }

        static function addScripts()
        {

        }

        static function addDropdownMenu($html, $item)
        {
            return $html;
        }

        static function addBodyClass($classes)
        {

            return $classes;
        }

        static function loginStyles()
        {
            ?>
            <style type="text/css">
                h1 a
                {
                    background: url('<?=get_template_directory_uri()?>/img/logoLogin.png') no-repeat center center !important; height:100px !important;
                    -webkit-background-size: auto auto !important; background-size: auto auto !important;width:200px !important;
                }

                #login { padding:80px 0 0 !important; }
            </style>
            <?php
        }
    }

    TrueTheme::init();


    class PrimeCustomSitemap
    {
        private static $sitemapName = 'activities';

        private static $itemsPerPage = -1;

        static function init()
        {
            add_action('init', array(__CLASS__, 'registerSitemap'));
            //Add to index
            add_filter('wpseo_sitemap_index', array(__CLASS__, 'addSitemapToIndex'), 10, 1);
        }

        /* Items Per Page
        /* ------------------------------- */
        static function getItemsPerPage()
        {
            $options = WPSEO_Options::get_all();
            return $options['entries-per-page'];
        }

        /* Generate SiteMap
        /* --------------------------- */
        static function generateThoroughbredSiteMap()
        {
            global $wpseo_sitemaps, $wpdb;

            $sitemap = $wpseo_sitemaps;

            $output = '';

            $xml = '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" ';
            $xml .= 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" ';
            $xml .= 'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";

            //Paging
            $maxEntries = self::getItemsPerPage();
            $steps  = $maxEntries;

            $n = get_query_var( 'sitemap_n' );

            if ( is_scalar( $n ) && intval( $n ) > 0 ) {
                $n = intval( $n );
            } else {
                $n = 1;
            }

            $offset = ( $n > 1 ) ? ( $n - 1 ) * $maxEntries : 0;

            $objectList = $wpdb->get_results( "SELECT id, name, updated_at FROM animal WHERE animal_cat = 1 AND status = 'PUBLISHED' ORDER BY id LIMIT $offset, $steps " );

            if(count($objectList) > 0)
            {
                foreach($objectList as $object)
                {
                    $output .= $sitemap->sitemap_url(
                        array(
                            'loc' => get_home_url() . '/animal/thoroughbred/' . $object->id,
                            'pri' => '0.8',
                            'chf' => 'weekly',
                            'mod' => $object->updated_at,
                        )
                    );
                }
            }

            $xml .= $output;
            $xml .= '</urlset>';

            $sitemap->set_sitemap($xml);
        }

        /* Generate SiteMap
        /* --------------------------- */
        static function generateGreyhoundSiteMap()
        {
            global $wpseo_sitemaps, $wpdb;

            $sitemap = $wpseo_sitemaps;

            $output = '';

            $xml = '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" ';
            $xml .= 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" ';
            $xml .= 'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";

            //Paging
            $maxEntries = self::getItemsPerPage();
            $steps  = $maxEntries;

            $n = get_query_var( 'sitemap_n' );

            if ( is_scalar( $n ) && intval( $n ) > 0 ) {
                $n = intval( $n );
            } else {
                $n = 1;
            }

            $offset = ( $n > 1 ) ? ( $n - 1 ) * $maxEntries : 0;

            $objectList = $wpdb->get_results( "SELECT id, name, updated_at FROM animal WHERE animal_cat = 2 AND status = 'PUBLISHED' ORDER BY id LIMIT $offset, $steps " );

            if(count($objectList) > 0)
            {
                foreach($objectList as $object)
                {
                    $output .= $sitemap->sitemap_url(
                        array(
                            'loc' => get_home_url() . '/animal/greyhound/' . $object->id,
                            'pri' => '0.8',
                            'chf' => 'weekly',
                            'mod' => $object->updated_at,
                        )
                    );
                }
            }

            $xml .= $output;
            $xml .= '</urlset>';

            $sitemap->set_sitemap($xml);
        }


        /* Generate SiteMap
        /* --------------------------- */
        static function generateStandardbredSiteMap()
        {
            global $wpseo_sitemaps, $wpdb;

            $sitemap = $wpseo_sitemaps;

            $output = '';

            $xml = '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" ';
            $xml .= 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" ';
            $xml .= 'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";

            //Paging
            $maxEntries = self::getItemsPerPage();
            $steps  = $maxEntries;

            $n = get_query_var( 'sitemap_n' );

            if ( is_scalar( $n ) && intval( $n ) > 0 ) {
                $n = intval( $n );
            } else {
                $n = 1;
            }

            $offset = ( $n > 1 ) ? ( $n - 1 ) * $maxEntries : 0;

            $objectList = $wpdb->get_results( "SELECT id, name, updated_at FROM animal WHERE animal_cat = 3 AND status = 'PUBLISHED' ORDER BY id LIMIT $offset, $steps " );

            if(count($objectList) > 0)
            {
                foreach($objectList as $object)
                {
                    $output .= $sitemap->sitemap_url(
                        array(
                            'loc' => get_home_url() . '/animal/harness/' . $object->id,
                            'pri' => '0.8',
                            'chf' => 'weekly',
                            'mod' => $object->updated_at,
                        )
                    );
                }
            }

            $xml .= $output;
            $xml .= '</urlset>';

            $sitemap->set_sitemap($xml);
        }

        static function registerSitemap()
        {
            global $wpseo_sitemaps;


            //Is yoast around?
            if($wpseo_sitemaps === null) return;

            $wpseo_sitemaps->register_sitemap('thoroughbreds', array(__CLASS__, 'generateThoroughbredSiteMap'));
            $wpseo_sitemaps->register_sitemap('greyhounds', array(__CLASS__, 'generateGreyhoundSiteMap'));
            $wpseo_sitemaps->register_sitemap('standardbreds', array(__CLASS__, 'generateStandardbredSiteMap'));
        }


        /* Add Sitemap to List
        /* ----------------------------------- */
        static function addSitemapToIndex($str)
        {
            global $wpdb;

            $itemsPerPage = self::getItemsPerPage();
            $output = '';

            //THOROUGHBREDS

            //Get the total number of listings
            $totalActivities = $wpdb->get_var( "SELECT COUNT(id) as total FROM animal WHERE animal_cat = 1 AND status = 'PUBLISHED'" );
            $n = ( $totalActivities > $itemsPerPage ) ? (int) ceil( $totalActivities / $itemsPerPage ) : 1;
            //Paging
            $maxEntries = $itemsPerPage;
            $steps  = $maxEntries;

            for($i = 1; $i <= $n; $i++)
            {
                $offset = ( $i > 1 ) ? ( $i - 1 ) * $maxEntries : 0;
                $updated_at = $wpdb->get_var( "SELECT updated_at FROM (SELECT * FROM animal WHERE animal_cat = 1 AND  status = 'PUBLISHED' LIMIT $offset, $steps) as tmpTable ORDER BY updated_at DESC" );
                $output .= '<sitemap>
                        <loc>' . get_home_url() . '/thoroughbreds-sitemap' . $i . '.xml</loc>
                        <lastmod>' . $updated_at . '</lastmod>
                        </sitemap>';
            }


            //GREYHOUNDS

            //Get the total number of listings
            $totalActivities = $wpdb->get_var( "SELECT COUNT(id) as total FROM animal WHERE animal_cat = 2 AND status = 'PUBLISHED'" );
            $n = ( $totalActivities > $itemsPerPage ) ? (int) ceil( $totalActivities / $itemsPerPage ) : 1;
            //Paging
            $maxEntries = $itemsPerPage;
            $steps  = $maxEntries;

            for($i = 1; $i <= $n; $i++)
            {
                $offset = ( $i > 1 ) ? ( $i - 1 ) * $maxEntries : 0;
                $updated_at = $wpdb->get_var( "SELECT updated_at FROM (SELECT * FROM animal WHERE animal_cat = 2 AND status = 'PUBLISHED' LIMIT $offset, $steps) as tmpTable ORDER BY updated_at DESC" );
                $output .= '<sitemap>
                        <loc>' . get_home_url() . '/greyhounds-sitemap' . $i . '.xml</loc>
                        <lastmod>' . $updated_at . '</lastmod>
                        </sitemap>';
            }

            //HARNESS

            //Get the total number of listings
            $totalActivities = $wpdb->get_var( "SELECT COUNT(id) as total FROM animal WHERE animal_cat = 3 AND status = 'PUBLISHED'" );
            $n = ( $totalActivities > $itemsPerPage ) ? (int) ceil( $totalActivities / $itemsPerPage ) : 1;
            //Paging
            $maxEntries = $itemsPerPage;
            $steps  = $maxEntries;

            for($i = 1; $i <= $n; $i++)
            {
                $offset = ( $i > 1 ) ? ( $i - 1 ) * $maxEntries : 0;
                $updated_at = $wpdb->get_var( "SELECT updated_at FROM (SELECT * FROM animal WHERE animal_cat = 3 AND status = 'PUBLISHED' LIMIT $offset, $steps) as tmpTable ORDER BY updated_at DESC" );
                $output .= '<sitemap>
                        <loc>' . get_home_url() . '/standardbreds-sitemap' . $i . '.xml</loc>
                        <lastmod>' . $updated_at . '</lastmod>
                        </sitemap>';
            }

            return $output;
        }
    }

    PrimeCustomSitemap::init();
