'use strict';

module.exports = function(grunt) {

  /**
   * Variables Definition!!!
   */
  var f = grunt.file.readJSON('grunt-vars.json');

  grunt.initConfig({
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        force: true
      },
      all: [
        f.jshint.all
      ]
    },
    less: {
      website: {
        files: {
          'assets/css/main.min.css': [
            'assets/less/app.less'
          ],
          // 'assets/css/woo.min.css': [
          //   'assets/less/woocommerce/woocommerce.less'
          // ]
        },
        options: {
          compress: true,
          // LESS source map
          // To enable, set sourceMap to true and update sourceMapRootpath based on your install
          sourceMap: false,
          sourceMapFilename: 'assets/css/main.min.css.map',
          sourceMapRootpath: '/wp-content/themes/true-theme/',
          message: 'Less compiled'
        }
      },
      app: {
        files: {
          'app/trueadmin/css/main.min.css': [
            'app/trueadmin/less/app.less'
          ]
        },
        // options: {
        //   compress: true,
        //   // LESS source map
        //   // To enable, set sourceMap to true and update sourceMapRootpath based on your install
        //   sourceMap: true,
        //   sourceMapFilename: 'app/trueadmin/css/main.min.css.map',
        //   sourceMapRootpath: '/wp-content/themes/sads-theme/app/trueadmin',
        //   message: 'APP - Less compiled'
        // }
      }
    },
    cssmin: {
      website: {
        files: {
          'assets/css/custom.min.css': ['assets/less/css/*.css'],
          // 'app/trueadmin/css/custom/min.css' : ['app/trueadmin/']
        }
      },
      app: {
        options: {
          keepSpecialComments: 0,
          banner: '/** Vendor - Minified **/'
        },
        files: {
          'app/trueadmin/css/_vendor.min.css': [
            f.cssmin.app
          ]
        }
      }
    },
    uglify: {
      website: {
        files: {
          'assets/js/scripts.min.js': [
            f.uglify.website
          ]
        },
        options: {
          // JS source map: to enable, uncomment the lines below and update sourceMappingURL based on your install
          //sourceMap: 'assets/js/scripts.min.js.map',
          //sourceMappingURL: '/wp-content/themes/sads-theme/assets/js/scripts.min.js.map'
        }
      },
      app: {
        files: {
          'app/trueadmin/js/scripts.min.js': [
            'app/trueadmin/js/src/core-util.js',
            'app/trueadmin/js/src/core.js',
            'app/trueadmin/js/src/page-scripts/*.js',
            'app/trueadmin/js/src/*.js'
          ]
        },
        options: {
          // JS source map: to enable, uncomment the lines below and update sourceMappingURL based on your install
          //sourceMap: 'assets/js/scripts.min.js.map',
          //sourceMappingURL: '/wp-content/themes/sads-theme/assets/js/scripts.min.js.map'
          beautify: {
            beautify: true,
            width: 80,
            },
          mangle: false
        }
      },
      app_vendor: {
        files: {
          'app/trueadmin/js/vendor.min.js': [
            f.uglify.app
          ]
        }
      }
    },
    version: {
      options: {
        file: 'app/roots/scripts.php',
        css: 'assets/css/main.min.css',
        cssHandle: 'roots_main',
        js: 'assets/js/scripts.min.js',
        jsHandle: 'roots_scripts'
      }
    },
    notify: {
      less: {
        options: {
          message: 'Less compiled'
        }
      },
      watch: {
        options: {
          message: 'Watch process: all completed'
        }
      }
    },
    bless: {
      app: {
        options: {
          cacheBuster : true
        },
        files: {
          'app/trueadmin/css/vendor.min.css' : 'app/trueadmin/css/_vendor.min.css'
        }
      }
    },
    watch: {
      less: {
        files: [
          'grunt-files.json',
          'assets/less/*.less',
          'assets/less/bootstrap/*.less',
          'assets/less/woocommerce/*.less'
        ],
        tasks: ['less:website', 'version']
      },
      css: {
        files: [
          'grunt-files.json',
          'assets/less/css/*.css',
        ],
        tasks: ['cssmin:website', 'version']
      },
      js: {
        files: [
          '<%= jshint.all %>'
        ],
        tasks: ['uglify:website', 'version']
      },
      // livereload: {
      //   // Browser live reloading
      //   // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
      //   options: {
      //     livereload: true
      //   },
      //   files: [
      //     f.livereload
      //   ]
      // },
      // app_less: {
      //   files: [
      //     'grunt-files.json',
      //     'app/trueadmin/less/*.less',
      //     'app/trueadmin/less/include/*.less',
      //   ],
      //   tasks: ['less:app']
      // },
      // app_css: {
      //   files: [
      //     'grunt-files.json',
      //     f.cssmin.app,
      //   ],
      //   tasks: ['cssmin:app', 'bless:app']
      // },
      // app_js: {
      //   files: [
      //     'grunt-files.json',
      //     'app/trueadmin/js/**/*.js',
      //     '!app/trueadmin/js/scripts.min.js',
      //     '!app/trueadmin/js/vendor.min.js',
      //   ],
      //   tasks: ['uglify:app']
      // },
      // app_js_vendor: {
      //   files: [
      //     'grunt-files.json',
      //     f.uglify.app
      //   ],
      //   tasks: ['uglify:app_vendor']
      // }
    },
    clean: {
      dist: [
        'assets/css/main.min.css',
        'assets/js/scripts.min.js',
        'app/trueadmin/js/scripts.min.css',
      ]
    },
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  // grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-css-metrics');
  grunt.loadNpmTasks('grunt-bless');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-wp-version');

  // Register tasks
  grunt.registerTask('default', [
    'clean',
    'less',
    'cssmin',
    'uglify',
    'version',
    // 'notify'
  ]);
  grunt.registerTask('dev', [
    'watch'
  ]);

};
