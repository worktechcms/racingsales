<?php

    class TrueModuleBase
    {
        var $moduleName = '';
        var $index = '';
        var $menuOptions = array();
        var $restricted = true;
        var $hasSettingsPage = true;
        var $hasMenuItem = true;
        var $hasErrors = false;
        var $errors = array();
        var $trueSettings = array();
        var $isPostBack = false;
        
        function __construct() 
        {
            
        }
        
        function openForm($formID = 'trueform')
        {
            ?>
            <form <?php if($formID) echo "id=\"$formID\"";?> action="admin.php?page=<?=$this->moduleName?>" method="post">
                <input type="hidden" name="is-post-back" value="true" />
            <?php
        }
        
        function closeform()
        {
            ?>
                <div class="true-field buttons">
                    <label>&nbsp;</label>
                    <input type="submit" class="button button-primary button-large" value="Update" />
                </div>
               
            </form>
            <?php
        }
        
        function getIndex()
        {
            return $this->index;
        }
        
        function enqueueStyles()
        {
            //Not all Modules have their own styles, so just put this here to keep php happy.   
        }
        
        function render()
        {
            ?>
            <div class="wrap">
                <div id="icon-edit-pages" class="icon32 icon32-posts-page"><br></br></div>
                <h2>True Agency</h2> 
            <?php
            
        }
                
        function showError($form, $fieldName, $printTags = true)
        {
           
            if($this->hasErrors)
            {
                
                if(isset($this->errors[$form][$fieldName]))
                {
                    if($printTags) echo '<p class="true-error">'; 
                    echo $this->errors[$form][$fieldName]['message'];
                    if($printTags) echo '</p>';
                }
            }
        }
        
        function getDefaultValue($form, $fieldName, $defaultText = '')
        {
            //If they haven't entered a default value, return whatever it should be
            if($this->hasErrors)
            {

                if(isset($this->errors[$form][$fieldName]))
                {
                    echo $this->errors[$form][$fieldName]['submittedValue'];
                    return;
                }
            } 
            
            echo $defaultText;  
            
        }
    }
?>