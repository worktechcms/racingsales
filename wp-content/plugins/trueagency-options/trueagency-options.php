<?php
/*
    Plugin Name: True Agency Options
    Plugin URI: www.trueagency.com.au
    Description: Configures options for True Agency websites
    Version: 1.0
    Author: True Agency
    Author URI: www.trueagency.com.au
*/

    class TrueAgencyOptions
    {
        var $Helper = null;
        var $home = null;
        
        var $modules = array();
        var $adminPermissionList = array();
        
        var $settings = array();
        
        
        public function initialise()
        {
            
            //Needs to be outside of required objects since we need to know if our page needs to be loaded or not
            include 'classes/module-base.php';
            $this->initialiseSettings();
            //Create our page array
            $enabledModules = $this->getEnabledModules();

            $this->adminPermissionList = $this->getAdminPermissionList();
            //Include our classes
            $this->loadModules($enabledModules);
            
            if(is_admin())
            {
                //Setup our navigation
                
                require_once 'modules/home/home.php';
                $this->home = new TrueHome($this->settings);
                add_action('admin_menu', array( $this, 'createNavigation'));
            }
            
            if($this->isOurPluginPage())
            {
                $this->loadRequiredObjects();
                $this->handlePostBack();
            }
                       
        }

        function getEnabledModules()
        {
            return get_option('true-enabled-modules', array()); 
        }
        
        function getAdminPermissionList()
        {
            return get_option('true-admin-enabled-modules', array()); 
        }
        
        function enableModule($module)
        {
            $enabledModules = get_option('true-enabled-modules', array());
            if(!in_array($module, $enabledModules))
            { 
                $enabledModules[] = $module;                       
                update_option('true-enabled-modules', $enabledModules);
            }
        }
        
        function disableModule($module)
        {
            $enabledModules = get_option('true-enabled-modules', array());
            foreach($enabledModules as $index => $var)
            {
                if($var == $module)
                {
                    unset($enabledModules[$index]); //Disable our module!
                    break;
                }
            }
            
            update_option('true-enabled-modules', $enabledModules);
        }
        
        function allowAdminAccess($module)
        {
            $adminPerms = get_option('true-admin-enabled-modules', array());
            if(!in_array($module, $adminPerms))
            { 
                $adminPerms[] = $module;                       
                update_option('true-admin-enabled-modules', $adminPerms);
            }
        }
        
        function disableAdminAccess($module)
        {
            $adminPerms = get_option('true-admin-enabled-modules', array());
            foreach($adminPerms as $index => $var)
            {
                if($var == $module)
                {
                    unset($adminPerms[$index]); //Disable our module!
                    break;
                }
            }
            
            update_option('true-admin-enabled-modules', $adminPerms);
        }

        public function isOurPluginPage()
        {
            if(is_admin())
            {
                if($module = $this->getCurrentPage())
                {
                    return true;
                    
                }

            }
            return false;
        }
        
        public function initialiseSettings()
        {

            $settings['plugin_title'] = 'True Agency Options';
            $settings['plugin_menu_title'] = 'True Agency';
            $settings['plugin_slug'] = 'trueagency';
            $settings['plugin_url'] = plugin_dir_url(__FILE__);
            $settings['plugin_menu_image'] = plugins_url() . '/trueagency-options/images/true-agency-logo.png';
            $settings['plugin_dir'] = __DIR__ . '/';
            $settings['module_dir'] = $settings['plugin_dir'] . 'modules/';
            $settings['module_url'] = $settings['plugin_url'] . 'modules/';
            $this->settings = $settings;
        }
        
        public function loadModules($modules)
        {
            if(count($modules) > 0)
            {
                foreach($modules as $module)
                {
                    $class = 'True' . ucfirst(strtolower($module));
                    
                    //Load our module and create a new instance of it
                    require_once 'modules/' . $module . '/' . $module . '.php';
    
                    $tmpModule = new $class($this->settings);
                    $index = $tmpModule->getIndex();
                    
                    //Check if the user has permission to access this module
                    if(in_array($module, $this->adminPermissionList))
                    {
                        $tmpModule->restricted = false;
                    }
                    
                    if((!$tmpModule->restricted) || $this->isTrueAgency())
                    {
                        //Add the page
                        $this->modules[$index] = $tmpModule;     
                    }
                }
            }
        }
        
        public function isTrueAgency()
        {
            $User = wp_get_current_user();
            if($User->user_login == 'trueagency' && current_user_can( 'manage_options' ))
            {
                return true;
            }
            return false;
        }
        
        function sortModulesByMenu($a, $b)
        {
            $optionsa = $a->getSubMenuOptions();
            $optionsb = $b->getSubMenuOptions();
            return strcasecmp($optionsa['menu_title'], $optionsb['menu_title']);
        }
        
        public function createNavigation()
        {            
            //Main navigation
            if($this->isTrueAgency() || count($this->modules) > 0)
            {
                add_menu_page(  $this->settings['plugin_title'], $this->settings['plugin_menu_title'], 'manage_options', 
                                $this->settings['plugin_slug'], array($this->home, 'initialise'), $this->settings['plugin_menu_image'], 3);
                //Sub Navigation
                if(count($this->modules) > 0)
                {
                    //Sort our modules by their menu title!
                    uasort($this->modules, array($this, 'sortModulesByMenu'));
                    
                    //Add the Modules to the Admin Menu
                    foreach($this->modules as $index => $module)
                    {
                        if($module->hasSettingsPage)
                        {
                            $options = $module->getSubMenuOptions();
                            $parentSlug = $this->settings['plugin_slug'];
                            if(!$module->hasMenuItem)
                            {
                                $parentSlug = null;
                            }
                            add_submenu_page(   $parentSlug, 
                                                $options['heading'], 
                                                $options['menu_title'], 
                                                $options['permissions'], 
                                                $options['slug'], 
                                                array($module, $options['function']));
                        }
                    }
                }
                //We need to call this anyway for the Home Module
                add_action( 'admin_enqueue_scripts', array($this, 'enqueueModuleScripts'));
            }
            
        }

        public function enqueueModuleScripts()
        {
            if($module = $this->getCurrentPage())
            {
                $module->enqueueStyles();
            }
        }
        
        function getCurrentPage()
        {
            if(isset($_GET['page']))
            {

                $moduleSlug = $_GET['page'];
                
                if($moduleSlug != 'trueagency')
                {
                    foreach($this->modules as $module)
                    {
                        if($module->moduleName == $moduleSlug)
                        {
                            return $module;
                        }
                    }
                } else {
                    return $this->home;
                }
            }
            return null;
        }
        
        public function handlePostBack()
        {
            if(isset($_POST['is-post-back']) || isset($_GET['is-post-back']))
            {
                $User = wp_get_current_user();
                
                if($module = $this->getCurrentPage())
                {       
                    if((!$module->restricted) || $this->isTrueAgency())
                    {
                        $module->isPostBack = true;
                        $module->handlePostBack();
                    }
                }
                 
            }
        }
        
        function isModuleEnabledByIndex($index)
        {
            foreach($this->modules as $module)
            {
                if($module->index == $index)
                {
                    return true;
                }
            }
            return false;
        }
        
        function isModuleTrueOnlyByIndex($index)
        {
            $adminPerms = get_option('true-admin-enabled-modules', array());
            if(!in_array($index, $adminPerms))
            {
                return true;
            }
            
            return false;
        }
        
        function getModuleByIndex($index)
        {
            foreach($this->modules as $module)
            {
                if($module->index == $index)
                {
                    return $module;
                }
            }
            
            return null;
        }
        
        function loadRequiredObjects()
        {
            //helper functions
            require_once 'classes/helper-functions.php';
            $this->Helper = new TrueHelperFunctions();
            //css
            wp_enqueue_style( 'trueoptionsstyles', plugins_url() . '/trueagency-options/css/truestyles.css');
            //javascript
            wp_enqueue_script('trueoptions', plugins_url() . '/trueagency-options/js/trueoptions.js', array( 'jquery'));
        }
        
    }

    require_once 'modules/true-settings/true-settings.php';
    
    $TrueAgencyOptions = new TrueAgencyOptions();
    add_action('init', array( $TrueAgencyOptions, 'initialise' ));
?>