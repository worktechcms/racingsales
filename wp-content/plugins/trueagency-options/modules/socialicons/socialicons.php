<?php

    include 'socialicon-api.php';

    class TrueSocialicons extends TrueModuleBase
    {
        var $index = 'socialicons';
        var $moduleName = 'true-socialicons';        
        var $modUrl = '';
        //Custom Variables
        var $enabledServices = null;
        
        function __construct($settings = null) 
        {
            parent::__construct();
            $this->trueSettings = $settings;   
            $this->modUrl = $this->trueSettings['module_url'] . 'socialicons/';
        }
        
        function getSubMenuOptions()
        {
            return array('heading' => 'Social Icons',
                         'menu_title' => 'Social Icons',
                         'permissions' => 'moderate_comments',
                         'slug' => $this->moduleName,
                         'function' => 'initialise');
        }
        
        function initialise()
        {

            $this->render();
        }
        
        public function getSupportedServices()
        {
            $services = array('facebook' => array( 'name' => 'Facebook',
                                'int-name' => 'facebook',
                                'code-small' => '<a class="addthis_button_facebook"></a>',
                                'code-medium' => '<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>',
                                'class' => 'facebook',
                                'admin-image' => 'facebook.png'),
                                
                        'twitter' =>  array( 'name' => 'Twitter',
                                'int-name' => 'twitter',
                                'code-small' => '<a class="addthis_button_twitter" data-counturl="{count_url}"></a>',
                                'code-medium' => '<a class="addthis_button_tweet" data-counturl="{count_url}"></a>',
                                'class' => 'twitter',
                                'admin-image' => 'twitter.png'),
                                
                        'linkedin' => array( 'name' => 'LinkedIn',
                                'int-name' => 'linkedin',
                                'code-small' => '<a class="addthis_button_linkedin"></a>',
                                'code-medium' => '<a class="addthis_button_linkedin_counter"></a>',
                                'class' => 'linkedin',
                                'admin-image' => 'linkedin.png'),
                                
                        'email' => array( 'name' => 'Email',
                                'int-name' => 'email',
                                'code-small' => '<a class="addthis_button_email"></a>',
                                'class' => 'email',
                                'admin-image' => 'email.png'),
                   'googleplus' => array('name' => 'Google+',
                                'int-name' => 'googleplus',
                                'code-small' => '<a class="addthis_button_google_plusone_share"></a>',
                                'code-medium' => '<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>',
                                'admin-image' => 'google-plus.png') 
                                );
                                
                               
                                
            uasort($services, array($this, 'sortServices'));
            return $services;                        
        }

        function sortServices($a, $b)
        {
            return strcasecmp($a['name'], $b['name']);
        } 

        
        
        function render()
        {
            $services = $this->getSupportedServices();
            $enabledServices = $this->getEnabledServices();

            ?>
            <div class="wrap">
                <h2>Social Icons</h2>
                <div class="trueopt-container">
                    <div class="trueopt-content">
                        <?php $this->openForm(); ?>
                            <?php
                                foreach($services as $service)
                                {
                                    $name = $service['name'];
                                    $imageFilename = $service['admin-image'];
                                    $accountTitle = $service['account-title'];
                                    $id = $service['int-name'];
                                    
                                    $enabled = isset($enabledServices[$id]);
                                    
                                    ?>
                                    <h3><img src="<?=$this->modUrl?>images/32px/<?=$imageFilename?>" style="margin-left:8px;vertical-align:middle;margin-top:-5px;margin-right:10px;"><?=$name?></h3>
                                    <div class="true-field">
                                        <label for="<?=$id?>-enabled">Enabled</label>
                                        
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="<?=$id?>-enabled" class="onoffswitch-checkbox" id="<?=$id?>-enabled" <?php if($enabled) { ?>checked<?php }?>>
                                            <label class="onoffswitch-label" for="<?=$id?>-enabled">
                                                <div class="onoffswitch-inner"></div>
                                                <div class="onoffswitch-switch"></div>
                                            </label>
                                        </div> 
                                    </div>
                                    <hr class="true-rule" />
                                    <?php
                                }
                            ?>
                            <div class="true-field">
                                <label></label>
                                <input type="submit" class="trueopt-button primary large" value="Update" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
        }
        
        function handlePostBack()
        {
            $services = $this->getSupportedServices();
            $enabledServices = array();
            foreach($services as $service)
            {
                $id = $service['int-name'];
                
                if(isset($_POST[$id . '-enabled']))
                {
                     $newService = array();
                     $newService['name'] = $id;
                     $enabledServices[$id] = $newService;
                }
            }
            
            $this->updateEnabledServices($enabledServices);
        }
        
        function updateEnabledServices($services)
        {
            $this->enabledServices = $services;
            update_option('true-socialicons-enabled', $services);
        }
        
        public function getEnabledServices()
        {
            if(!$this->enabledServices) $this->enabledServices = get_option('true-socialicons-enabled', array());
            return $this->enabledServices;
        }
    }

?>