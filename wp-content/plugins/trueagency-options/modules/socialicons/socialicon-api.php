<?php
    
    class TrueSocialIconAPI
    {
        private static  $enabledServices = null;
        private static  $services = null;
        private static $includedScript = false;
        
        var $size = 'small';
        
        function getEnabledServices()
        {
            if(!self::$enabledServices) self::$enabledServices = get_option('true-socialicons-enabled', array());
            return self::$enabledServices;
        }
        
        function initServices()
        {
            if(self::$enabledServices == null)
            {
                $iconlist = new TrueSocialicons();
                self::$services = $iconlist->getSupportedServices();
                self::$enabledServices = $iconlist->getEnabledServices();
            }
        }
        
        function setSize($size)
        {
            $this->size = $size;
        }
        
        public function showIcons($customURL = '', $customTitle = '')
        {
            ob_start();
            $this->initServices();
            
            $attributes = '';
            if($customURL != '')
            {
                $attributes = 'addthis:url="' . $customURL . '" ';
                $attributes .= ' addthis:counturl="' . $customURL . '" ';
            }
            
            if($customTitle != '')
            {
                $attributes .= 'addthis:title="' . $customTitle .'"';
            }

            $sizeCode = ' addthis_16x16_style';
            if($this->size == 'medium')
            {
                $sizeCode = '';
            }
            ?>
            <div class="share-container">
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style<?=$sizeCode?>" <?=$attributes?>>
                    <?php
                        
                        foreach(self::$enabledServices as $service)
                        {
                            
                            $serviceDetails = self::$services[$service['name']];
                            if($serviceDetails['name'] == 'Twitter' && $customURL != '')
                            {
                                echo str_replace('{count_url}', $customURL, $serviceDetails['code-' . $this->size]);
                                
                            } else {
                                if($serviceDetails['name'] == 'Twitter')
                                {
                                    echo str_replace('data-counturl="{count_url}"', '', $serviceDetails['code-' . $this->size]);  
                                } else {
                                    echo $serviceDetails['code-' . $this->size];
                                }
                            }
                        }
                        
                        if($this->size == 'small')
                        {
                            ?>
                            <a class="addthis_counter addthis_bubble_style"></a>
                            <?php
                        }
                    ?>
                </div>
            </div>
            <?php
                if(!self::$includedScript)
                {
                    ?>
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js"></script>
                    <?php
                }
            ?>
            <!-- AddThis Button END -->
            
            <?php
            self::$includedScript = true;
            $icons = ob_get_clean();
            return $icons;
        }
    }

?>