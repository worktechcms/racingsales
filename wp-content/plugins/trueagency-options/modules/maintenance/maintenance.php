<?php

    class TrueMaintenance extends TrueModuleBase
    {
        var $activeTab = 'maintenance';
        var $index = 'maintenance';
        var $moduleName = 'true-maintenance';        
        var $forms = array();
        var $globalVarsTable = null;
        var $stylesIncluded = false;
        
        function __construct($settings = null) 
        {
            parent::__construct();
            $this->trueSettings = $settings;   
            
            $isMaintenanceMode = get_option('true-maintenance-enabled');

            if($isMaintenanceMode != '')
            {
                $this->displayMaintenanceMode();
            }
            
        }
        
        function initialise()
        {
            $maintMessage = get_option('true-maintenance-msg', 'This website is currently undergoing maintenance. Please try again later.');
            $enableMaint = get_option('true-maintenance-enabled', '');
            $imageUrl = get_option('true-maintenance-image', '');                 
            $this->forms['maintenance'] = array(   'enable_maint' => $enableMaint,
                                                    'maint_text' => $maintMessage,
                                                    'ad_image' => $imageUrl
                                            );
            $this->render();
        }
        
        function enqueueStyles()
        {
            if(!$this->stylesIncluded)
            {
                wp_enqueue_style( 'truemaintenancecss', $this->trueSettings['module_url'] . 'maintenance/maintenance.css');
                $this->stylesIncluded = true;
            }
        }
        
        
        
        function getSubMenuOptions()
        {
            return array('heading' => 'Maintenance',
                         'menu_title' => 'Maintenance',
                         'permissions' => 'moderate_comments',
                         'slug' => $this->moduleName,
                         'function' => 'initialise');
        }
        
        
        function render()
        {
            
            ?>
            <div class="wrap">
               
                <h2>Maintenance</h2>
                
                <?php $this->openForm(); ?>
                <div class="true-form oddeven">
                    <input type="hidden" name="subform" value="maintenance" />
                    <div class="true-field">
                        <label>Maintenance Mode</label>
                        <input type="checkbox" name="enable_maint" <?php $this->getDefaultValue('maintenance', 'enable_maint', $this->forms['maintenance']['enable_maint']); ?> value="enable" />
                    </div>
                    <div class="true-field">
                        <label>Message</label>
                        <textarea name="maint_text"><?php $this->getDefaultValue('maintenance', 'maint_text', $this->forms['maintenance']['maint_text']); ?></textarea>
                        <?php $this->showError('maintenance', 'maint_text'); ?>
                    </div>
                    <div class="true-field">
                        <label>Image</label>
                        <input id="upload_image" type="text" class="large" size="36" name="ad_image" value="<?php $this->getDefaultValue('maintenance', 'ad_image', $this->forms['maintenance']['ad_image']); ?>" />
                        <input id="upload_image_button" class="button button-primary" type="button" value="Upload Image" />
                        <p class="true-description">Enter a URL or upload an image</p>
                    </div>
                </div>   
                <?php $this->closeForm(); ?>

            </div>    
            <?php
            
        }
        
        function handlePostBack()
        {
            
            $subform = '';
            if(isset($_POST['subform']))
            {
                $subform = $_POST['subform'];
            } else {
                if(isset($_GET['subform']))
                {
                    $subform = $_GET['subform'];
                }
            }
            
            
            
            if($subform != '')
            {
                switch($subform)
                {
                    case 'maintenance':
                        $this->handleTab1();
                        break;
                }            
            }
        }
        
        function handleTab1()
        {
            $maintMessage = $_POST['maint_text'];
            $enableMaint = $_POST['enable_maint'];
            $imageURL = $_POST['ad_image'];

            $errList['test'] = array(   'submittedValue' => 'someval',
                                        'message' => 'Oh noes you broke it');
            $this->errors = $errList;
            //$this->hasErrors = true;
            
            if(!$this->hasErrors)
            {
                update_option('true-maintenance-msg', $maintMessage );

                if($enableMaint == 'enable') 
                {
                    $enableMaint = 'checked="checked"';
                    add_action('admin_bar_menu', array( $this, "addMaintenanceToMenuBar" ), 10 );     
                } else {
                    $enableMaint = '';
                    add_action('admin_bar_menu', array( $this, "removeMaintenanceToMenuBar" ), 11 );   
                }
                update_option('true-maintenance-enabled', $enableMaint );
                update_option('true-maintenance-image', $imageURL );
            }
           
        }

        function throwError($form, $field, $message, $value)
        {
            $errList[$field] = array(   'submittedValue' => $value,
                                        'message' => $message);
            $this->errors[$form] = $errList;
            $this->hasErrors = true;
        }
        
        function displayMaintenanceMode()
        {
            $this->enqueueStyles();
            $User = wp_get_current_user();
            
            
            add_action('admin_bar_menu', array( $this, "addMaintenanceToMenuBar" ), 10 );
            add_filter( 'login_message', array( &$this, 'login_message' ) );                                
            //Do we need to display the maintenance page?
            if(!current_user_can( 'manage_options' ))
            {
                if( strstr($_SERVER['PHP_SELF'], 'wp-login.php' ) 
                    || strstr( $_SERVER['PHP_SELF'], 'async-upload.php' ) // Otherwise media uploader does not work 
                    || strstr( htmlspecialchars( $_SERVER['REQUEST_URI'] ), '/plugins/' ) // So that currently enabled plugins work while in maintenance mode.
                    || strstr( $_SERVER['PHP_SELF'], 'upgrade.php' )
                    
                )
                { 
                    return;
                }
                
                $image = get_option('true-maintenance-image');      
                $maintenanceMessage = get_option('true-maintenance-msg');
                
                include $this->trueSettings['module_dir'] . 'maintenance/templates/maintenance.php';
                die;
            }
        }

        function addMaintenanceToMenuBar()
        {
            global $wp_admin_bar;
            //Add the button to the top nav bar
            $wp_admin_bar->add_menu( array( 'parent' => '', 
                                            'id' => 'true-admin-maint', 
                                            'title' => 'Maintenance Mode Activated', 
                                            'href' => admin_url('admin.php?page=true-maintenance')) );
        }
        
        function removeMaintenanceToMenuBar()
        {
            global $wp_admin_bar;
            $wp_admin_bar->remove_menu('true-admin-maint');
        }
        
        public function login_message () 
        {
            return '<div id="login_error"><p>Maintenance Mode enabled. If you are an Admin and are experiencing difficulties logging in please contact True Agency.</p></div>' . "\n";
        } // End login_message()
    }
?>