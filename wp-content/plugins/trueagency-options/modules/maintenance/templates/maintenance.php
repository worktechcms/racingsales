<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Maintenance Mode</title>
        <style type="text/css">
            body 
            {
                
                background: #f9f9f9;
                color: #333;
                font-family: sans-serif;
    
                padding: 1em 2em;
            }
            
            .container
            { 
                width:390px; margin: 2em auto; background: #fff;
                -webkit-border-radius: 3px;
                border-radius: 3px;
                border: 1px solid #dfdfdf;
                padding: 1em 2em;
                margin-top: 50px;
                box-shadow: 3px 5px 5px rgba(0, 0, 0, 0.5);
                
            }
            h2 { text-align:center; }

            img {  width: 100px;margin:auto;display: block; }

           
            p {
                font-size: 14px;
                line-height: 1.5;
                margin: 25px 0 20px;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <?php if($image != '') { ?><img src="<?=$image?>"><?php } ?>
            <h2>Maintenance Mode</h2>
            <p><?=$maintenanceMessage?></p>
        </div>
    </body>
</html>