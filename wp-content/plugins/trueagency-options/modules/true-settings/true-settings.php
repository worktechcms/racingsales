<?php

    $trueSettingsDir = plugins_url() . '/trueagency-options/modules/true-settings';
    
    add_filter('wpseo_metabox_prio', function() { return 'low';}); 
    remove_action('wpcf7_admin_notices', 'wpcf7_welcome_panel', 2);
        
    function setDefaultACFOptions($options, $postID)
    {
        $options['layout'] = 'default';
        return $options;
    }
    add_filter('acf/field_group/get_options', 'setDefaultACFOptions', 100, 2);
    
    /*****************/
    /* Login Rebrand */
    /*****************/
    
    function trueLoginLogoUrl()
    {
        return site_url();
    }
    add_filter('login_headerurl', 'trueLoginLogoUrl');
    
    /**************************/
    /* WooCommerce Rebranding */
    /**************************/

    add_action( 'admin_menu', 'true_rebrand_admin_menu', 10 );

      /**
       * true_rebrand_admin_menu()
       *
       * Rebrand the admin menu label.
       *
       * @since 1.0.0
       */
      function true_rebrand_admin_menu () {
        global $menu, $trueSettingsDir;

        if ( is_array( $menu ) ) {
            foreach ( $menu as $k => $v ) {
                if ( $v[0] == 'TrueCommerce') 
                {
                    $menu[$k][4] = str_replace('menu-icon-generic', 'menu-icon-truecommerce_menu', $menu[$k][4]);
                    $menu[$k][6] = $trueSettingsDir . '/images/true-agency-logo.png';
                    break;
                }
            }
        }
        
        $User = wp_get_current_user();
        if($User->user_login != 'trueagency')
        {
            $killer = null;
            //Since backupbuddy refuses to die, it needs a seperate bit of code
            while ($menu_name = current($menu))
            {
                if ($menu_name[0] == 'BackupBuddy') 
                {
                    $killer = key($menu);
                    break;
                }
                next($menu);
            }
            if($killer)
                unset($menu[$killer]);
        }
      } // End rebrand_admin_menu()


    /*******************************/
    /* Override WooCommerce Styles */
    /*******************************/
    
    add_action( 'admin_enqueue_scripts', 'true_override_css', 10 );
    function true_override_css () 
    {
        global $trueSettingsDir;
        wp_register_style( 'truecommerce-branding', $trueSettingsDir . '/css/truecommerce-style.css', '', '1.0.0', 'screen' );
        wp_enqueue_style( 'truecommerce-branding' );
    } // End true_override_css()
    
    
    function true_custom_dashboard_widgets() {  
        global $wp_meta_boxes;  
      
        wp_add_dashboard_widget('custom_help_widget', 'True Agency', 'custom_dashboard_true');  
    }  
      
    function custom_dashboard_true() {
        global $trueSettingsDir;
        echo '<div class="true_widget_logo" style="float:left; width:190px;"><a href="http://www.trueagency.com.au"><img alt="True Agency" src="' . $trueSettingsDir . '/images/logo.png" /></a></div><div class="true_widget_detail" style="font-family:helvetica;min-height:170px;"><strong>Welcome to your Dashboard</strong><br/><br/>For support, please contact us by emailing <a href="mailto:support@trueagency.com.au">support@trueagency.com.au</a> or calling 03 9529 1850. Please note that support for any plugins/extensions/code not implemented by True Agency will be quoted separately.<br/><br/><a href="http://www.trueagency.com.au">True Agency</a> specialise in websites, ecommerce and mobile apps.</div>';  
    }  
      
    add_action('wp_dashboard_setup', 'true_custom_dashboard_widgets');  
    
    /**********************/
    /* Clean up Dashboard */  
    /**********************/
    function disable_default_dashboard_widgets() 
    {  
        remove_meta_box('dashboard_right_now', 'dashboard', 'core');  
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'core');  
        remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  
        remove_meta_box('dashboard_plugins', 'dashboard', 'core');  
        remove_meta_box('dashboard_quick_press', 'dashboard', 'core');  
        remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');  
        remove_meta_box('dashboard_primary', 'dashboard', 'core');  
        remove_meta_box('dashboard_secondary', 'dashboard', 'core');  
        remove_meta_box('dashboard_activity', 'dashboard', 'core');
    }  
    add_action('admin_menu', 'disable_default_dashboard_widgets'); 
    
    /***********************************************/
    /*   Remove Various Things from normal users   */
    /***********************************************/
    
    function remove_admin_bar_links() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
//        $wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
//        $wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
//        $wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
//        $wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
//        $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
//        $wp_admin_bar->remove_menu('site-name');        // Remove the site name menu
//        $wp_admin_bar->remove_menu('view-site');        // Remove the view site link
//        $wp_admin_bar->remove_menu('updates');          // Remove the updates link
//        $wp_admin_bar->remove_menu('comments');         // Remove the comments link
//        $wp_admin_bar->remove_menu('new-content');      // Remove the content link
//        $wp_admin_bar->remove_menu('my-account');       // Remove the user details tab
        $wp_admin_bar->remove_menu('themes');            
        $wp_admin_bar->remove_menu('customize');            
        $wp_admin_bar->remove_menu('widgets');      
        $wp_admin_bar->remove_menu('wpseo-menu');      

    }
    add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );
    
    /**********************************/
    /*    Remove Various Menu Items   */
    /**********************************/


    /**********************************/
    /*       Remove Help Button       */
    /**********************************/
    add_filter( 'contextual_help', 'true_remove_help_menu', 999, 3 );
    function true_remove_help_menu($old_help, $screen_id, $screen)
    {
        $screen->remove_help_tabs();
        return $old_help;
    }
        
    /**********************************/
    /*    Remove Nag / Meta Notices   */
    /**********************************/
    add_action( 'admin_init', 'true_remove_nag');
    function true_remove_nag()
    {
        remove_action( 'admin_notices', 'update_nag', 3 );
        remove_action( 'admin_notices', 'woothemes_updater_notice' );
        remove_meta_box('rg_forms_dashboard', 'dashboard', '');
        remove_meta_box('pb_backupbuddy_stats', 'dashboard', '');
        remove_meta_box('woocommerce_dashboard_recent_reviews', 'dashboard', '');
    }
    
    function trueRemoveMetaBoxes()
    {
        $User = wp_get_current_user();
        
    }

    add_action( 'add_meta_boxes', 'trueRemoveMetaBoxes', 999);
    

    /**********************************/
    /*      Hide Wordpress Version    */
    /**********************************/
    add_filter( 'update_footer', 'true_remove_footer_version', 9999 );
    add_action( 'admin_head', 'true_hide_wp_version');
    
    function true_hide_wp_version()
    {
        echo '<style type="text/css">#wp-version-message { display: none;}</style>';
    }
    
    function true_remove_footer_version()
    {
        return '';
    }
    
    add_action( 'admin_init', 'rebrand_admin_settings', 10 );
    function rebrand_admin_settings () {

        $tabs = array( 'general', 'page', 'catalog', 'inventory', 'shipping', 'tax', 'email', 'integration' );
        foreach ( $tabs as $k => $v ) 
        {
            add_filter( 'woocommerce_' . $v . '_settings', 'replace_brand_name', 10 );
        }

    } // End rebrand_admin_settings()

    function replace_brand_name ( $fields ) 
    {
        foreach ( $fields as $k => $v ) 
        {
            if ( isset( $v['desc'] ) ) 
            {
                $fields[$k]['desc'] = str_replace( 'WooCommerce', 'TrueCommerce', $fields[$k]['desc'] );
            }
            if ( isset( $v['name'] ) ) 
            {
                $fields[$k]['name'] = str_replace( 'WooCommerce', 'TrueCommerce', $fields[$k]['name'] );
            }
        
        }
        return $fields;
    }

    add_filter( 'gettext', 'true_gettext', 10, 3 );
    function true_gettext( $translated, $text, $domain )
    {
        if ( strstr( $text, 'WooCommerce' )  ) 
        {
            $translated = str_replace( 'WooCommerce', 'TrueCommerce', $translated );
        }
        
        return $translated;
    }

    add_action('wp_before_admin_bar_render', 'true_before_adminbar', 0);
    function true_before_adminbar() 
    {
        echo '<style type="text/css"> .postbox-container .meta-box-sortables.empty-container, #side-sortables.empty-container{border:0;} </style>';
    }

    if (isset($wp_version)) 
    {
        add_filter("mce_buttons", "extended_editor_mce_buttons", 999);
    }

    function extended_editor_mce_buttons($buttons)
    {
        if (($key = array_search('woocommerce_shortcodes_button', $buttons)) !== FALSE) 
        {
            unset($buttons[$key]);
        }
        
        return $buttons;
    }
        