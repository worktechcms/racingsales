<?php

    class TrueHome extends TrueModuleBase
    {
        var $activeTab = 'home';
        var $moduleName = 'home';
        var $index = 'home';
        var $modDir = '';

        function __construct($settings = null) 
        {
            parent::__construct();
            $this->trueSettings = $settings;
            $this->modDir = $this->trueSettings['module_url'] . 'home/';
        }
        
        function initialise()
        {
            $this->render();   
        }
                        
        function render()
        {
            global $TrueAgencyOptions;
            $TAO = $TrueAgencyOptions;
            $isTrueAgency = $TAO->isTrueAgency();
            $moduleList = $this->getModules();

            ?>
            <div class="wrap">
                <h2>True Agency Options</h2>
                
                <div class="module-wrapper">
                    <?php 
                        $i = 0;
                        foreach($moduleList as $module)
                        {
                            
                            $modObj = $module['module-obj'];
                            $moduleSlug = '';
                            $hasSettingsPage = false;
                            if($modObj != null)
                            {
                                $moduleSlug = $modObj->moduleName;
                                $hasSettingsPage = $modObj->hasSettingsPage;  
                            }
                            if(!$isTrueAgency && $module['true-only'])
                            {
                                continue;
                            }
                            $moduleIndex = $module['index'];
                            ?>
                            <div class="module">
                                <h3><?=$module['title']?></h3>
                                <p>
                                    <?=$module['description']?>
                                </p>
                                <div class="button-container">
                                    <?php
                                        if(!$module['enabled'] && $isTrueAgency)
                                        {
                                            ?>
                                            <a href="admin.php?page=trueagency&is-post-back=true&enable=true&module=<?=$moduleIndex?>" class="trueopt-button primary">
                                                Enable
                                            </a>
                                            <?php
                                        } else {
                                            //Only show the disable button for us
                                            if($isTrueAgency)
                                            {
                                                ?>
                                                <a href="admin.php?page=trueagency&is-post-back=true&enable=false&module=<?=$moduleIndex?>" class="trueopt-button error">
                                                    Disable
                                                </a>
                                                <?php
                                            }
                                            
                                            if($hasSettingsPage)
                                            {
                                                ?>
                                                <a href="admin.php?page=<?=$moduleSlug?>" class="trueopt-button">
                                                    Settings
                                                </a>
                                                <?php
                                            }
                                            
                                            if($isTrueAgency)
                                            {
                                                if($module['true-only'])
                                                {
                                                    ?>
                                                    <a href="admin.php?page=trueagency&is-post-back=true&allowadmin=true&module=<?=$moduleIndex?>" class="trueopt-button warning">
                                                        Allow Client Access
                                                    </a>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <a href="admin.php?page=trueagency&is-post-back=true&allowadmin=false&module=<?=$moduleIndex?>" class="trueopt-button success">
                                                        Disable Client Access
                                                    </a>
                                                    <?php
                                                }
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                    ?>
                    <div class="clr"></div>
                </div> <!-- /.module-wrapper -->
            </div>
            <?php
        }
        
        function enqueueStyles()
        {
            wp_enqueue_style('truehomecss', $this->modDir . 'styles.css');
            wp_enqueue_script('truehomejs', $this->modDir . 'home.js', array( 'jquery'));
        }
        
        function getModules()
        {
            global $TrueAgencyOptions;
            $TAO = $TrueAgencyOptions;
            
            $modules = array();
            if ($handle = opendir($this->trueSettings['module_dir']))
            {
                $blacklist = array('.', '..', 'home');
                while (false !== ($file = readdir($handle))) 
                {
                    if(is_dir($this->trueSettings['module_dir'] . $file))
                    {
                        if (!in_array($file, $blacklist))
                        {
                            if(file_exists($this->trueSettings['module_dir'] . $file . '/description.txt'))
                            {
                                $moduleDetails = json_decode(file_get_contents($this->trueSettings['module_dir'] . $file . '/description.txt'));
                                $module['title'] = $moduleDetails->title;
                                $module['description'] = $moduleDetails->description;
                                $module['image'] = $moduleDetails->image;
                                $module['index'] = $moduleDetails->index;
                                $module['filename'] = $file;
                                //get the description                            
                                
                                $module['enabled'] = $TAO->isModuleEnabledByIndex($file);
                                $module['true-only'] = $TAO->isModuleTrueOnlyByIndex($file);
                                $module['module-obj'] = $TAO->getModuleByIndex($file);
                                //Add the module to the list
                                $modules[] = $module;
                            }
                        }
                    }
                }
                closedir($handle);
            }

            

            if(count($modules) > 1)
            {
                uasort($modules, array($this, 'sortModules'));
            }
            
            return $modules;
        }
        
        function sortModules($a, $b)
        {
            return strcasecmp($a['title'], $b['title']);
        }
        
        function handlePostBack()
        {
            global $TrueAgencyOptions;
            $TAO = $TrueAgencyOptions;
            
            if($TAO->isTrueAgency())
            {
                //Process our Form!
                if(isset($_GET['enable']))
                {
                    //We're enabling a module
                    if($_GET['enable'] == 'true')
                    {
                        $TAO->enableModule($_GET['module']);
                    } else {
                        $TAO->disableModule($_GET['module']);
                    }
                } else if(isset($_GET['allowadmin']))
                {
                    //We're allowing admin access to a module
                    if($_GET['allowadmin'] == 'true')
                    {
                        $TAO->allowAdminAccess($_GET['module']);
                    } else {
                        $TAO->disableAdminAccess($_GET['module']);
                    }
                }
            }

            wp_redirect( get_admin_url(null, 'admin.php?page=trueagency'));

        }
    }
?>