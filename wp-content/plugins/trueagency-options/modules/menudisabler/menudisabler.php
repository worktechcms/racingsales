<?php

    class TrueMenudisabler extends TrueModuleBase
    {
        var $index = 'menudisabler';
        var $moduleName = 'true-menudisabler';        
        var $hasMenuItem = true;
        var $uneditedMenu = null;
        var $removedItems = null;
        var $adminBarItemsToRemove = array();
        
        function __construct($settings = null) 
        {
            parent::__construct();
            $this->trueSettings = $settings;   
            add_action( 'admin_menu', array(&$this, 'editAdminMenu'), 999);
        }
        
        function editAdminMenu() 
        {
             global $menu;
             $this->uneditedMenu = $menu;    
             $this->removeMenuItems();
        }
        
        function removeMenuItems()
        {
            global $TrueAgencyOptions;
            
            if(!$TrueAgencyOptions->isTrueAgency())
            {
                $itemsToRemove = $this->getRemovedItems();
                foreach($itemsToRemove as $menuItem)
                {
                    remove_menu_page($menuItem);
                    
                    //Do we need to remove any items from the admin bar?
                    switch($menuItem)
                    {
                        case 'plugins.php':
                            $this->adminBarItemsToRemove[] = 'updates';
                            break;
                        case 'edit-comments.php':
                            $this->adminBarItemsToRemove[] = 'comments';
                            
                            break;                        
                    }
                }
                
                //If we have admin bar items to remove, setup the hook
                if(count($this->adminBarItemsToRemove) > 0)
                {
                    add_action( 'wp_before_admin_bar_render', array($this, 'removeAdminBarItems') );
                }
            }
        }
        
        function removeAdminBarItems()
        {
            global $wp_admin_bar; 
            
            foreach($this->adminBarItemsToRemove as $item)
            {
                $wp_admin_bar->remove_menu($item);    
            }
        }
        
        function getRemovedItems()
        {
            if(!$this->removedItems) 
            {
                //auto blocks
                // - acf
                // - themes
                // - plugins
                // - backupbuddy
                // - Yoast SEO
                $defaultRemovedItems = array();
                $this->removedItems = get_option('true-menu-editor-removed', $defaultRemovedItems);
            }
            return $this->removedItems;
        }
        
        function disableMenuItem($item)
        {
            $removedItems = $this->getRemovedItems();
            if(!in_array($item, $removedItems))
            {
                $removedItems[] = $item;
                $this->removedItems[] = $item;
                update_option('true-menu-editor-removed', $removedItems);
            }
        }
        
        function enableMenuItem($item)
        {
            $removedItems = $this->getRemovedItems();
            
            if(in_array($item, $removedItems))
            {
                foreach($removedItems as $index => $var)
                {
                    if($var == $item)
                    {

                        unset($removedItems[$index]);
                        unset($this->removedItems[$index]);
                        update_option('true-menu-editor-removed', $removedItems);
                        break;
                    }
                }
            }
        }
        
        function initialise()
        {
            $this->render();
        }

        function getSubMenuOptions()
        {
            return array('heading' => 'Menu Disabler',
                         'menu_title' => 'Admin Menu Disabler',
                         'permissions' => 'moderate_comments',
                         'slug' => $this->moduleName,
                         'function' => 'initialise');
        }
        
        function getParsedMenu()
        {

            $result = array();
            $menu = $this->uneditedMenu;

            foreach($menu as $menuItem)
            {
                $name = $menuItem[0];
                //Hide seperators and True Agency Options
                if($name != '' && $name != 'True Agency')
                {
                    if($menuItem[2] != 'edit-tags.php?taxonomy=link_category')
                    {
                        //Clean up a few names
                        if($menuItem[5] == 'menu-comments') $name = 'Comments';
                        if($menuItem[5] == 'menu-plugins') $name = 'Plugins';
                        $menuItem[0] = $name;
                        $result[] = $menuItem;
                    }
                }
                
            }
            return $result;
        }
        
        
        function render()
        {
            $menu = $this->getParsedMenu();
            $removedItems = $this->getRemovedItems();
            ?>
            <div class="wrap">
                <h2>Admin Menu Disabler</h2>
                <a href="admin.php?page=trueagency">Back to True Options</a>
                <div class="trueopt-container">
                    <div class="trueopt-content small" style="width:220px">
                        <div class="true-form">
                            <p>Enable or disable Admin Menu Items</p>
                            <?php $this->openForm(); ?>
                                <?php
                                    foreach($menu as $menuItem)
                                    {
                                        $name = $menuItem[0];
                                        //Hide seperators and True Agency Options
                                        $disabled = false;
                                        if(in_array($menuItem[2], $removedItems)) $disabled = true;
                                        $slug = str_replace(array('?', '.'), '', $menuItem[2]);

                                        ?>
                                        <div class="true-field">
                                            <label for="enabled-<?=$slug?>"><?=$name?></label>
                                            <div class="onoffswitch">
                                                <input type="checkbox" name="enabled-<?=$slug?>" class="onoffswitch-checkbox" id="enabled-<?=$slug?>" <?php if(!$disabled) { ?>checked<?php } ?>>
                                                <label class="onoffswitch-label" for="enabled-<?=$slug?>">
                                                    <div class="onoffswitch-inner"></div>
                                                    <div class="onoffswitch-switch"></div>
                                                </label>
                                            </div> 
                                        </div>
                                        <?php
                                    }
                                ?>                           
                                <hr class="true-rule" />
                                <div class="true-field">
                                    <label></label>
                                    <input type="submit" class="trueopt-button primary large" value="Update" />
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <?php
            
        }
        
        function handlePostBack()
        {
            //We have to wait for the admin menu loaded, since we need a list of all the admin items
            add_action( 'admin_menu', array(&$this, 'updateAdminMenu'), 1000);
        }
        
        function updateAdminMenu()
        {

            $menu = $this->getParsedMenu();
            foreach($menu as $menuItem)
            {
                //Get the converted version of the menu's slug
                $slug = 'enabled-' . str_replace(array('?', '.'), '', $menuItem[2]);
                if(!isset($_POST[$slug]))
                {
                    
                    $this->disableMenuItem($menuItem[2]);
                } else {
                    
                    //we might need to enable one of them, so better check!!
                    $this->enableMenuItem($menuItem[2]);
                }
            }
            
            $this->removeMenuItems();
            wp_redirect( get_admin_url(null, 'admin.php?page=' . $this->moduleName));

        }
    }
?>